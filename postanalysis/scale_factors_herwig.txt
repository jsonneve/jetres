created dir pdfy
Using data file "/afs/desy.de/user/s/sonnevej/res/output_res_data_fine_eta_analysis_Run2016BCD_Cert_271036-276811_13TeV_PromptReco_Collisions16_JSON_NoL1T_Spring16_25nsV6_L2L3Res/data_histograms_incl_full.root"
Using MC file "/afs/desy.de/user/s/sonnevej/res/output_res_mc_herwig_fine_eta_analysis_spring16_v2_13invfb69_PU_reweighted/herwig_histograms_incl_full.root"
Using data label "Data"
Using MC label "MC"
 FCN=9.5778 FROM MIGRAD    STATUS=CONVERGED     264 CALLS         265 TOTAL
                     EDM=2.01059e-07    STRATEGY= 1      ERROR MATRIX ACCURATE 
  EXT PARAMETER                                   STEP         FIRST   
  NO.   NAME      VALUE            ERROR          SIZE      DERIVATIVE 
   1  N           -3.74873e+00   1.04227e+00   5.00115e-04   1.21283e-03
   2  S            5.92224e-01   5.77384e-02   1.57390e-05  -2.09246e-02
   3  C            5.56404e-02   1.02977e-03   4.48355e-07  -5.44842e-01
 FCN=16.2684 FROM MIGRAD    STATUS=CONVERGED     188 CALLS         189 TOTAL
                     EDM=5.77165e-07    STRATEGY= 1      ERROR MATRIX ACCURATE 
  EXT PARAMETER                                   STEP         FIRST   
  NO.   NAME      VALUE            ERROR          SIZE      DERIVATIVE 
   1  N            4.64020e+00   4.96147e-01   3.22443e-04  -2.53018e-03
   2  S            6.18962e-01   3.20207e-02   1.10873e-05  -8.02051e-02
   3  C            5.56380e-02   5.47992e-04   2.83751e-07  -4.35443e+00
 FCN=12.4088 FROM MIGRAD    STATUS=CONVERGED     322 CALLS         323 TOTAL
                     EDM=2.3211e-08    STRATEGY= 1      ERROR MATRIX ACCURATE 
  EXT PARAMETER                                   STEP         FIRST   
  NO.   NAME      VALUE            ERROR          SIZE      DERIVATIVE 
   1  N            3.26518e+00   1.36343e+00   6.83545e-04  -3.23475e-04
   2  S            7.49407e-01   5.28856e-02   1.56324e-05  -8.14239e-03
   3  C            6.24939e-02   1.06345e-03   5.20397e-07  -1.89097e-01
 FCN=5.56052 FROM MIGRAD    STATUS=CONVERGED     218 CALLS         219 TOTAL
                     EDM=3.23123e-07    STRATEGY= 1      ERROR MATRIX ACCURATE 
  EXT PARAMETER                                   STEP         FIRST   
  NO.   NAME      VALUE            ERROR          SIZE      DERIVATIVE 
   1  N            3.96390e+00   6.67305e-01   2.51312e-04   1.72490e-03
   2  S            8.05863e-01   2.98600e-02   6.44665e-06   1.30443e-01
   3  C            6.06297e-02   6.29211e-04   2.12595e-07   3.97259e+00
standard scales, eta bin 0

****************************************
Minimizer is Linear
Chi2                      =      9.98448
NDf                       =            8
p0                        =     0.969981   +/-   0.0131362   


standard scales, eta bin 1

****************************************
Minimizer is Linear
Chi2                      =      5.54664
NDf                       =            7
p0                        =      1.02812   +/-   0.0256346   


standard scales, eta bin 2

****************************************
Minimizer is Linear
Chi2                      =      3.66457
NDf                       =            7
p0                        =      1.01138   +/-   0.0254163   


standard scales, eta bin 3

****************************************
Minimizer is Linear
Chi2                      =      4.11266
NDf                       =            7
p0                        =      1.00055   +/-   0.0403105   


standard scales, eta bin 4

****************************************
Minimizer is Linear
Chi2                      =      2.33208
NDf                       =            8
p0                        =      1.09693   +/-   0.0268054   


standard scales, eta bin 5

****************************************
Minimizer is Linear
Chi2                      =      6.52797
NDf                       =            8
p0                        =     0.968364   +/-   0.0731143   


standard scales, eta bin 6

****************************************
Minimizer is Linear
Chi2                      =      4.91956
NDf                       =            5
p0                        =       1.0137   +/-   0.0979925   


standard scales, eta bin 7

****************************************
Minimizer is Linear
Chi2                      =      2.64396
NDf                       =            4
p0                        =       1.1059   +/-   0.158734    


standard scales, eta bin 8

****************************************
Minimizer is Linear
Chi2                      =      1.24273
NDf                       =            3
p0                        =     0.958535   +/-   0.179807    


standard scales, eta bin 9

****************************************
Minimizer is Linear
Chi2                      =     0.903716
NDf                       =            2
p0                        =      1.21014   +/-   0.14095     


standard scales, eta bin 10


FE control scales, eta bin 0

****************************************
Minimizer is Linear
Chi2                      =      13.9296
NDf                       =            8
p0                        =      1.01568   +/-   0.00593503  


FE control scales, eta bin 1

****************************************
Minimizer is Linear
Chi2                      =      4.74342
NDf                       =            8
p0                        =      1.03388   +/-   0.00900513  


FE control scales, eta bin 2

****************************************
Minimizer is Linear
Chi2                      =      4.28286
NDf                       =            8
p0                        =      1.04511   +/-   0.0171384   


FE control scales, eta bin 3

****************************************
Minimizer is Linear
Chi2                      =      11.9172
NDf                       =            8
p0                        =       1.0847   +/-   0.0198401   


FE control scales, eta bin 4

****************************************
Minimizer is Linear
Chi2                      =      6.17007
NDf                       =            8
p0                        =      1.06705   +/-   0.0230866   


FE control scales, eta bin 5

****************************************
Minimizer is Linear
Chi2                      =      3.93065
NDf                       =            7
p0                        =      1.05703   +/-   0.0278634   


FE control scales, eta bin 6

****************************************
Minimizer is Linear
Chi2                      =      4.76965
NDf                       =            8
p0                        =       1.2147   +/-   0.0356616   


FE control scales, eta bin 7

****************************************
Minimizer is Linear
Chi2                      =      2.37521
NDf                       =            6
p0                        =      1.57559   +/-   0.071591    


FE control scales, eta bin 8

****************************************
Minimizer is Linear
Chi2                      =      11.0577
NDf                       =            5
p0                        =      1.11608   +/-   0.0412174   


FE control scales, eta bin 9

****************************************
Minimizer is Linear
Chi2                      =       1.0764
NDf                       =            4
p0                        =      1.01935   +/-   0.0431544   


standard with 0.15 scales, eta bin 0

****************************************
Minimizer is Linear
Chi2                      =      14.5976
NDf                       =            8
p0                        =     0.979472   +/-   0.0118691   


standard with 0.15 scales, eta bin 1

****************************************
Minimizer is Linear
Chi2                      =      2.03734
NDf                       =            8
p0                        =      1.03539   +/-   0.0231543   


standard with 0.15 scales, eta bin 2

****************************************
Minimizer is Linear
Chi2                      =      2.61503
NDf                       =            7
p0                        =      1.00517   +/-   0.0226914   


standard with 0.15 scales, eta bin 3

****************************************
Minimizer is Linear
Chi2                      =      3.29826
NDf                       =            7
p0                        =      1.01185   +/-   0.035479    


standard with 0.15 scales, eta bin 4

****************************************
Minimizer is Linear
Chi2                      =      2.00231
NDf                       =            8
p0                        =      1.07137   +/-   0.0225211   


standard with 0.15 scales, eta bin 5

****************************************
Minimizer is Linear
Chi2                      =      6.13172
NDf                       =            7
p0                        =     0.945199   +/-   0.0587726   


standard with 0.15 scales, eta bin 6

****************************************
Minimizer is Linear
Chi2                      =      1.68787
NDf                       =            5
p0                        =      1.03717   +/-   0.0805491   


standard with 0.15 scales, eta bin 7

****************************************
Minimizer is Linear
Chi2                      =      3.80389
NDf                       =            6
p0                        =      1.03365   +/-   0.10712     


standard with 0.15 scales, eta bin 8

****************************************
Minimizer is Linear
Chi2                      =      1.58664
NDf                       =            5
p0                        =      1.10863   +/-   0.138658    


standard with 0.15 scales, eta bin 9

****************************************
Minimizer is Linear
Chi2                      =     0.946998
NDf                       =            3
p0                        =      1.26561   +/-   0.121422    


standard with 0.15 scales, eta bin 10

****************************************
Minimizer is Linear
Chi2                      =   1.4249e-32
NDf                       =            0
p0                        =     0.786519   +/-   0.930076    


FE with 015 scales, eta bin 0

****************************************
Minimizer is Linear
Chi2                      =      0.98086
NDf                       =            8
p0                        =      1.02342   +/-   0.0318203   


FE with 015 scales, eta bin 1

****************************************
Minimizer is Linear
Chi2                      =     0.549597
NDf                       =            8
p0                        =      1.01009   +/-   0.0555005   


FE with 015 scales, eta bin 2

****************************************
Minimizer is Linear
Chi2                      =       2.0596
NDf                       =            8
p0                        =      1.06044   +/-   0.065767    


FE with 015 scales, eta bin 3

****************************************
Minimizer is Linear
Chi2                      =      1.22789
NDf                       =            8
p0                        =       1.0524   +/-   0.0712567   


FE with 015 scales, eta bin 4

****************************************
Minimizer is Linear
Chi2                      =      0.45581
NDf                       =            6
p0                        =      1.06464   +/-   0.089689    


FE with 015 scales, eta bin 5

****************************************
Minimizer is Linear
Chi2                      =       4.5058
NDf                       =            7
p0                        =      1.17805   +/-   0.0975007   


FE with 015 scales, eta bin 6

****************************************
Minimizer is Linear
Chi2                      =     0.362365
NDf                       =            4
p0                        =      1.63849   +/-   0.119891    


FE with 015 scales, eta bin 7

****************************************
Minimizer is Linear
Chi2                      =      9.33756
NDf                       =            5
p0                        =      1.15272   +/-   0.0485303   


FE with 015 scales, eta bin 8

****************************************
Minimizer is Linear
Chi2                      =      1.08198
NDf                       =            4
p0                        =     0.948367   +/-   0.0796304   


FE scales, eta bin 0

****************************************
Minimizer is Linear
Chi2                      =      1.10676
NDf                       =            8
p0                        =      1.04599   +/-   0.03121     


FE scales, eta bin 1

****************************************
Minimizer is Linear
Chi2                      =      1.72605
NDf                       =            8
p0                        =      1.06517   +/-   0.0615019   


FE scales, eta bin 2

****************************************
Minimizer is Linear
Chi2                      =      4.27752
NDf                       =            8
p0                        =      1.12655   +/-   0.0724777   


FE scales, eta bin 3

****************************************
Minimizer is Linear
Chi2                      =      3.27865
NDf                       =            8
p0                        =       1.0804   +/-   0.0734286   


FE scales, eta bin 4

****************************************
Minimizer is Linear
Chi2                      =      1.97585
NDf                       =            7
p0                        =      1.07941   +/-   0.0855474   


FE scales, eta bin 5

****************************************
Minimizer is Linear
Chi2                      =      1.40533
NDf                       =            7
p0                        =      1.42533   +/-   0.128763    


FE scales, eta bin 6

****************************************
Minimizer is Linear
Chi2                      =     0.436353
NDf                       =            3
p0                        =      1.74852   +/-   0.170375    


FE scales, eta bin 7

****************************************
Minimizer is Linear
Chi2                      =      8.66861
NDf                       =            5
p0                        =      1.14291   +/-   0.056212    


FE scales, eta bin 8

****************************************
Minimizer is Linear
Chi2                      =     0.811851
NDf                       =            4
p0                        =      1.04148   +/-   0.0802278   


Fit function at bin 10 was not found
(int)0
dir plots_herwig already exists
will overwrite all files
