standard scales, eta bin 0

****************************************
Minimizer is Linear
Chi2                      =      2.98737
NDf                       =            6
p0                        =      1.09754   +/-   0.0195182   


standard scales, eta bin 1

****************************************
Minimizer is Linear
Chi2                      =     0.230866
NDf                       =            6
p0                        =      1.13025   +/-   0.0357699   


standard scales, eta bin 2

****************************************
Minimizer is Linear
Chi2                      =      2.32374
NDf                       =            6
p0                        =      1.13498   +/-   0.0354069   


standard scales, eta bin 3

****************************************
Minimizer is Linear
Chi2                      =      1.15993
NDf                       =            6
p0                        =      1.00648   +/-   0.0509474   


standard scales, eta bin 4

****************************************
Minimizer is Linear
Chi2                      =      5.40447
NDf                       =            7
p0                        =      1.10516   +/-   0.0313431   


standard scales, eta bin 5

****************************************
Minimizer is Linear
Chi2                      =      2.18154
NDf                       =            6
p0                        =      1.04714   +/-   0.0960224   


standard scales, eta bin 6

****************************************
Minimizer is Linear
Chi2                      =      1.55318
NDf                       =            4
p0                        =     0.997942   +/-   0.121942    


standard scales, eta bin 7

****************************************
Minimizer is Linear
Chi2                      =      1.43678
NDf                       =            5
p0                        =     0.972132   +/-   0.152495    


standard scales, eta bin 8

****************************************
Minimizer is Linear
Chi2                      =     0.245471
NDf                       =            3
p0                        =      1.29024   +/-   0.492511    


standard scales, eta bin 9

****************************************
Minimizer is Linear
Chi2                      =     0.439796
NDf                       =            2
p0                        =      1.43433   +/-   0.348369    


standard scales, eta bin 10
Warning in <Fit>: Fit data is empty 


FE control scales, eta bin 0

****************************************
Minimizer is Linear
Chi2                      =      5.58436
NDf                       =            8
p0                        =      1.10617   +/-   0.00806887  


FE control scales, eta bin 1

****************************************
Minimizer is Linear
Chi2                      =      3.04765
NDf                       =            8
p0                        =      1.11957   +/-   0.0111601   


FE control scales, eta bin 2

****************************************
Minimizer is Linear
Chi2                      =      3.61492
NDf                       =            8
p0                        =      1.09144   +/-   0.0190697   


FE control scales, eta bin 3

****************************************
Minimizer is Linear
Chi2                      =      2.05889
NDf                       =            8
p0                        =       1.1399   +/-   0.0231565   


FE control scales, eta bin 4

****************************************
Minimizer is Linear
Chi2                      =      7.89704
NDf                       =            7
p0                        =      1.09915   +/-   0.0255651   


FE control scales, eta bin 5

****************************************
Minimizer is Linear
Chi2                      =      4.40287
NDf                       =            7
p0                        =      1.15008   +/-   0.0322711   


FE control scales, eta bin 6

****************************************
Minimizer is Linear
Chi2                      =      14.9506
NDf                       =            8
p0                        =      1.19667   +/-   0.0336212   


FE control scales, eta bin 7

****************************************
Minimizer is Linear
Chi2                      =      4.79437
NDf                       =            5
p0                        =      1.34252   +/-   0.0689466   


FE control scales, eta bin 8

****************************************
Minimizer is Linear
Chi2                      =      6.96789
NDf                       =            4
p0                        =      1.36524   +/-   0.0360076   


FE control scales, eta bin 9

****************************************
Minimizer is Linear
Chi2                      =      3.19491
NDf                       =            4
p0                        =      1.32877   +/-   0.0405798   


standard with 0.15 scales, eta bin 0

****************************************
Minimizer is Linear
Chi2                      =      3.11482
NDf                       =            7
p0                        =       1.0572   +/-   0.0169075   


standard with 0.15 scales, eta bin 1

****************************************
Minimizer is Linear
Chi2                      =     0.819083
NDf                       =            7
p0                        =      1.03895   +/-   0.0302759   


standard with 0.15 scales, eta bin 2

****************************************
Minimizer is Linear
Chi2                      =       1.9157
NDf                       =            6
p0                        =      1.05279   +/-   0.0294019   


standard with 0.15 scales, eta bin 3

****************************************
Minimizer is Linear
Chi2                      =      1.02438
NDf                       =            7
p0                        =       0.9906   +/-   0.0429426   


standard with 0.15 scales, eta bin 4

****************************************
Minimizer is Linear
Chi2                      =      4.87427
NDf                       =            8
p0                        =      1.05514   +/-   0.0254476   


standard with 0.15 scales, eta bin 5

****************************************
Minimizer is Linear
Chi2                      =      1.39277
NDf                       =            6
p0                        =      1.02381   +/-   0.0804613   


standard with 0.15 scales, eta bin 6

****************************************
Minimizer is Linear
Chi2                      =     0.631559
NDf                       =            6
p0                        =     0.999555   +/-   0.0878303   


standard with 0.15 scales, eta bin 7

****************************************
Minimizer is Linear
Chi2                      =      1.68754
NDf                       =            5
p0                        =     0.981353   +/-   0.103625    


standard with 0.15 scales, eta bin 8

****************************************
Minimizer is Linear
Chi2                      =    0.0353276
NDf                       =            2
p0                        =      1.12506   +/-   0.185745    


standard with 0.15 scales, eta bin 9

****************************************
Minimizer is Linear
Chi2                      =     0.140662
NDf                       =            2
p0                        =      1.25927   +/-   0.160644    


standard with 0.15 scales, eta bin 10
Warning in <Fit>: Fit data is empty 


FE with 015 scales, eta bin 0

****************************************
Minimizer is Linear
Chi2                      =      2.59438
NDf                       =            8
p0                        =      1.06822   +/-   0.0191736   


FE with 015 scales, eta bin 1

****************************************
Minimizer is Linear
Chi2                      =     0.762026
NDf                       =            8
p0                        =      1.00212   +/-   0.0349934   


FE with 015 scales, eta bin 2

****************************************
Minimizer is Linear
Chi2                      =      1.52255
NDf                       =            8
p0                        =      1.11214   +/-   0.047454    


FE with 015 scales, eta bin 3

****************************************
Minimizer is Linear
Chi2                      =      2.26495
NDf                       =            7
p0                        =      1.02888   +/-   0.0476221   


FE with 015 scales, eta bin 4

****************************************
Minimizer is Linear
Chi2                      =       1.0064
NDf                       =            7
p0                        =      1.08537   +/-   0.0633554   


FE with 015 scales, eta bin 5

****************************************
Minimizer is Linear
Chi2                      =      5.98697
NDf                       =            8
p0                        =      1.12027   +/-   0.0643295   


FE with 015 scales, eta bin 6

****************************************
Minimizer is Linear
Chi2                      =      0.91407
NDf                       =            4
p0                        =      1.55646   +/-   0.0874446   


FE with 015 scales, eta bin 7

****************************************
Minimizer is Linear
Chi2                      =      2.25884
NDf                       =            4
p0                        =      1.46384   +/-   0.0422297   


FE with 015 scales, eta bin 8

****************************************
Minimizer is Linear
Chi2                      =      1.62562
NDf                       =            4
p0                        =      1.26328   +/-   0.0558034   


FE scales, eta bin 0

****************************************
Minimizer is Linear
Chi2                      =      2.19903
NDf                       =            8
p0                        =      1.12666   +/-   0.0230138   


FE scales, eta bin 1

****************************************
Minimizer is Linear
Chi2                      =       2.5981
NDf                       =            8
p0                        =      1.07169   +/-   0.0421441   


FE scales, eta bin 2

****************************************
Minimizer is Linear
Chi2                      =      1.72143
NDf                       =            8
p0                        =      1.17392   +/-   0.0578041   


FE scales, eta bin 3

****************************************
Minimizer is Linear
Chi2                      =      3.57258
NDf                       =            6
p0                        =      1.09455   +/-   0.059193    


FE scales, eta bin 4

****************************************
Minimizer is Linear
Chi2                      =      2.00716
NDf                       =            6
p0                        =      1.20184   +/-   0.0804757   


FE scales, eta bin 5

****************************************
Minimizer is Linear
Chi2                      =        5.122
NDf                       =            5
p0                        =      1.23078   +/-   0.0822834   


FE scales, eta bin 6

****************************************
Minimizer is Linear
Chi2                      =      3.07353
NDf                       =            4
p0                        =      1.44884   +/-   0.116132    


FE scales, eta bin 7

****************************************
Minimizer is Linear
Chi2                      =      5.46769
NDf                       =            4
p0                        =      1.41222   +/-   0.0466783   


FE scales, eta bin 8

****************************************
Minimizer is Linear
Chi2                      =       1.4879
NDf                       =            4
p0                        =      1.45253   +/-   0.0761107   


