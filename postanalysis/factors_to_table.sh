#!/usr/bin/env zsh

sed -e 's/ 3 //g' -e 's/  /\&/g' -e 's/$/\\\\\\hline/g' Fall15_25nsV2_MC_SF_AK4PFchs_corrected.txt  | grep -v '-' > Fall15_25nsV2_MC_SF_AK4PFchs_corrected.tex
sed -e 's/ 3 //g' -e 's/  /\&/g' -e 's/$/\\\\\\hline/g' Fall15_25nsV2_MC_SF_AK4PFchs.txt  | grep -v '-' | grep -iv 'scalefactor' > Fall15_25nsV2_MC_SF_AK4PFchs.tex
