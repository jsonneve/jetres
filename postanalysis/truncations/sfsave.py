"""
Save scale factors that are printed by iterFit.C
in a latex table format.

Note that you have the following scale factors:
    Standard 0-10
    FE Control 0-9
    Standard 0.15 0-10
    Forward Extension 0.15a 0-8
    Forward Extension 0-8

Use only bins 0-9 for the Standard (10 is shitty due
to statistics).
Also, Forward starts at eta 1.3 so you should shift the
bin numbers by 4.
Then combine all overlapping bins weighted, like
sqrt(standard**2*(1/dstandard)**2 + forward**2*(1/dforward)**2) * 1/sumuncertainties(?)
"""

import math
import glob
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

sep = '&\t'
eol = '\t\\\hline'

lumi = 589

bin_edges = {}

bin_edges[0] = "0.0& 0.5"
bin_edges[1] = "0.5& 0.8"
bin_edges[2] = "0.8& 1.1"
bin_edges[3] = "1.1& 1.3"
bin_edges[4] = "1.3& 1.7"
bin_edges[5] = "1.7& 1.9"
bin_edges[6] = "1.9& 2.1"
bin_edges[7] = "2.1& 2.3"
bin_edges[8] = "2.3& 2.5"
bin_edges[9] = "2.5& 2.8"
bin_edges[10] = "2.8& 3.0"
bin_edges[11] = "3.0& 3.2"
bin_edges[12] = "3.2& 4.7"





sf_computed_0 = {}
sf_computed_15 = {}
for fi in glob.glob('sf_trunc*.txt'):
    ofi = open(fi, 'r')
    scalefactors = {}
    l = ofi.readline()
    while 'eta bin' not in l:
        l = ofi.readline()
    while l != '' and not '(int)0' in l:
        tp, bi = l.split(', eta bin ')
        bi = int(bi)
        if tp not in scalefactors:
            scalefactors[tp] = {}
        l = ofi.readline()
        if 'warning' in l or 'Warning' in l:
            chi2 = None
            ndf = None
            scale = 0
            dscale = 0
            scalefactors[tp][bi] = {'chi2': chi2, 'SF': scale, 'dsf': dscale, 'ndf':
                ndf, 'SF+': scale + dscale, 'SF-': scale - dscale}
            l = ofi.readline()
            l = ofi.readline()
            l = ofi.readline()
            continue
        l = ofi.readline()
        l = ofi.readline()
        l = ofi.readline()
        chi2 = float(l.split()[-1])
        l = ofi.readline()
        ndf = int(l.split()[-1])
        l = ofi.readline()
        scale = float(l.split()[-3])
        dscale = float(l.split()[-1])
        scalefactors[tp][bi] = {'chi2': chi2, 'SF': scale, 'dsf': dscale, 'ndf':
                ndf, 'SF+': scale + dscale, 'SF-': scale - dscale}
        l = ofi.readline()
        l = ofi.readline()
        l = ofi.readline()

    trunc = fi[fi.rindex('sf_trunc') + 8:-3]
    sf_computed_0[trunc] = {}

    out_file = fi[:-3] + 'tex'
    outopen = open(out_file, 'w')
    # ETA bin 0-3 are computed with standard scales 0-3
    # ETA bin 4-9 are computed with standard scales 4-9 and forward scales 0-5
    # ETA bin 10-12 are with forward scales 6-8
    bin_combine_min = 4
    bin_combine_max = 9
    tyst = 'standard scales'
    tyfe = 'FE scales'
    eol = '\t\\\\\hline'
    
    for b in range(0, 13):
        if b < 11:
    
            sf_standard = scalefactors[tyst][b]['SF']
            sf_standard_unc = scalefactors[tyst][b]['dsf']
            sf = sf_standard
            sf_unc = sf_standard_unc
            computation = "standard"
        if b >= bin_combine_min:
    
            b_fe = b - 4
            sf_fe = scalefactors[tyfe][b_fe]['SF']
            sf_fe_unc = scalefactors[tyfe][b_fe]['dsf']
            sf_unc = sf_fe_unc
            computation = "forward ext."
            sf = sf_fe
            sf_unc = sf_fe_unc
            if b <= bin_combine_max:
    
                # Then combine all overlapping bins weighted, like
                # sqrt(standard**2*(1/dstandard)**2 + forward**2*(1/dforward)**2) * 1/sumuncertainties(?)
                #sf_unc = math.sqrt(sf_standard_unc**2 + sf_fe_unc**2)
                #sf_unc = math.sqrt(sf_standard_unc*sf_fe_unc/abs(sf_standard_unc -
                #    sf_fe_unc))
                if sf_unc == 0 or sf_standard == 0 or sf_fe == 0:
                    sf = 0
                    sf_unc = 0
                    computation = "None"
                else:
                    sf_unc = round(1/math.sqrt(1/sf_standard_unc**2 +
                        1/sf_fe_unc**2), 5)
                    sf_unc_inv = math.sqrt(1/sf_standard_unc**2 + 1/sf_fe_unc**2)
                    sf = round(math.sqrt(sf_standard**2*(1/sf_standard_unc)**2 +
                        sf_fe**2*(1/sf_fe_unc**2))/sf_unc_inv, 5)
                    computation = "standard + forward ext."
        sf_computed_0[trunc][b] = {'sf': sf, 'unc': sf_unc, 'method': computation, 'edges':
                bin_edges[b]}
        line = bin_edges[b] + '\t' + sep
        line += str(round(sf, 3)) + sep
        line += ' $\pm$ ' + str(round(sf_unc, 3)) + sep
        line += computation
        line += eol + '\n'
        outopen.write(line)
        #print  + ''.join(sep + str(round(scalefactors[ty][b][qt],3)) for qt in qty)\
        #        + eol
    
    outopen.close()
    out_file = fi[:-4] + '15.tex'
    sf_computed_15[trunc] = {}
    outopen = open(out_file, 'w')
    # ETA bin 0-3 are computed with standard scales 0-3
    # ETA bin 4-9 are computed with standard scales 4-9 and forward scales 0-5
    # ETA bin 10-12 are with forward scales 6-8
    bin_combine_min = 4
    bin_combine_max = 9
    tyst = 'standard with 0.15 scales'
    tyfe = 'FE with 015 scales'
    eol = '\t\\\\\hline'
    
    for b in range(0, 13):
        if b < 11:
    
            sf_standard = scalefactors[tyst][b]['SF']
            sf_standard_unc = scalefactors[tyst][b]['dsf']
            sf = sf_standard
            sf_unc = sf_standard_unc
            computation = "standard"
        if b >= bin_combine_min:
    
            b_fe = b - 4
            sf_fe = scalefactors[tyfe][b_fe]['SF']
            sf_fe_unc = scalefactors[tyfe][b_fe]['dsf']
            sf_unc = sf_fe_unc
            computation = "forward ext."
            sf = sf_fe
            sf_unc = sf_fe_unc
            if b <= bin_combine_max:
    
                # Then combine all overlapping bins weighted, like
                # sqrt(standard**2*(1/dstandard)**2 + forward**2*(1/dforward)**2) * 1/sumuncertainties(?)
                #sf_unc = math.sqrt(sf_standard_unc**2 + sf_fe_unc**2)
                #sf_unc = math.sqrt(sf_standard_unc*sf_fe_unc/abs(sf_standard_unc -
                #    sf_fe_unc))
                if sf_unc == 0 or sf_standard == 0 or sf_fe == 0:
                    sf = 0
                    sf_unc = 0
                    computation = "None"
                else:
                    sf_unc = round(1/math.sqrt(1/sf_standard_unc**2 +
                        1/sf_fe_unc**2), 5)
                    sf_unc_inv = math.sqrt(1/sf_standard_unc**2 + 1/sf_fe_unc**2)
                    sf = math.sqrt(sf_standard**2*(1/sf_standard_unc)**2 +
                        sf_fe**2*(1/sf_fe_unc**2))/sf_unc_inv
                    computation = "standard + forward ext."
        sf_computed_15[trunc][b] = {'sf': sf, 'unc': sf_unc, 'method': computation, 'edges':
                bin_edges[b]}
        line = bin_edges[b] + '\t' + sep
        line += str(round(sf, 3)) + sep
        line += ' $\pm$ ' + str(round(sf_unc, 3)) + sep
        line += computation
        line += eol + '\n'
        outopen.write(line)
        #print  + ''.join(sep + str(round(scalefactors[ty][b][qt],3)) for qt in qty)\
        #        + eol
    
    outopen.close()





 
## Plots
plt.figure()
# keys: ['100.', '085.', '080.', '090.', '095.', '0985.']
for trunc in ['080.', '085.', '090.', '095.', '100.', '0985.']: #sf_computed_0:
    x, y = zip(*[[i, sf_computed_0[trunc][i]['sf']] for  i in sf_computed_0[trunc]])
    x = [round((float(sf_computed_0[trunc][i]['edges'].split('&')[1]) +
        float(sf_computed_0[trunc][i]['edges'].split('&')[0]))/2, 1) for i in
        range(len(sf_computed_0[trunc]))]
    xerr = [round((float(sf_computed_0[trunc][i]['edges'].split('&')[1]) -
        float(sf_computed_0[trunc][i]['edges'].split('&')[0]))/2, 2) for i in
        range(len(sf_computed_0[trunc]))]
    #x5, y5 = zip(*[[i, sf_computed_15[i]['sf']] for  i in sf_computed_15])
    #x5 = [round((float(sf_computed_15[i]['edges'].split('&')[1]) +
    #    float(sf_computed_15[i]['edges'].split('&')[0]))/2, 1) for i in
    #    range(len(sf_computed_15))]
    #xerr5 = [round((float(sf_computed_15[i]['edges'].split('&')[1]) -
    #    float(sf_computed_15[i]['edges'].split('&')[0]))/2, 2) for i in
    #    range(len(sf_computed_15))]
    #plt.xticks(x, [sf_computed_0[i]['edges'].replace('&', '$< |\eta| <$') for i in
    #    x], rotation='vertical', fontsize=16)
    truncstring = trunc[0] + '.' + trunc[1:-1]
    label = '$\sigma^{\mathrm{data}}_{\mathrm{' + truncstring + '}}/\sigma^{\mathrm{MC}}_{\mathrm{' + truncstring + '}}$'
    #label5 = '$\sigma^{\mathrm{data}}_{\mathrm{0.15}}/\sigma^{\mathrm{MC}}_{\mathrm{0.15}})$'
    #plt.scatter(x, y, s=30, c='magenta', linewidths=0, label=label)
    plt.errorbar(x, y, xerr=xerr, yerr=[sf_computed_0[trunc][i]['unc'] for i
        in range(len(sf_computed_0[trunc]))],
            ls='none', elinewidth=2, fmt='--o',
            label=label)
    #plt.scatter(x5, y5, s=30, c='blue', linewidths=0, label=label5)
    #plt.errorbar(x5, y5, xerr=xerr5, yerr=[sf_computed_15[i]['unc'] for i in
    #    range(len(sf_computed_15))],
    #        ls='none', elinewidth=2, ecolor='blue', color='blue', fmt='--o', label=label5)
plt.ylim(0,2)
plt.xlim(0,5)
#plt.xlim(min(x) - 0.5, max(x) + 0.5)
plt.annotate('CMS Preliminary', (0.03, 0.9), fontweight='bold', fontsize=14,
        xycoords='axes fraction')
plt.annotate('$L=' + str(lumi) + '$ $\mathrm{pb}^{-1}$', (0.03, 0.8), fontsize=18, 
        xycoords='axes fraction')
plt.ylabel('Jet energy resolution scale factor', fontsize=14)
plt.xlabel('$|\eta|$', fontsize=16)
plt.axhline(y=1, color='orange', linewidth=2, linestyle='--')
plt.legend(loc='lower right', fontsize=20, ncol=2)
plotfilename = 'truncation_scale_factors.pdf'
plt.savefig(plotfilename, bbox_inches='tight')
