"""
Save scale factors that are printed by iterFit.C
in a latex table format.

Note that you have the following scale factors:
    Standard 0-10
    FE Control 0-9
    Standard 0.15 0-10
    Forward Extension 0.15a 0-8
    Forward Extension 0-8

Use only bins 0-9 for the Standard (10 is shitty due
to statistics).
Also, Forward starts at eta 1.3 so you should shift the
bin numbers by 4.
Then combine all overlapping bins weighted, like
sqrt(standard**2*(1/dstandard)**2 + forward**2*(1/dforward)**2) * 1/sumuncertainties(?)
"""

import math

fi = 'scale_factors.txt'
ofi = open(fi, 'r')
scalefactors = {}
l = ofi.readline()
while l != '':
    tp, bi = l.split(', eta bin ')
    bi = int(bi)
    if tp not in scalefactors:
        scalefactors[tp] = {}
    l = ofi.readline()
    if 'warning' in l or 'Warning' in l:
        chi2 = None
        ndf = None
        scale = 0
        dscale = 0
        scalefactors[tp][bi] = {'chi2': chi2, 'SF': scale, 'dsf': dscale, 'ndf':
            ndf, 'SF+': scale + dscale, 'SF-': scale - dscale}
        l = ofi.readline()
        l = ofi.readline()
        l = ofi.readline()
        continue
    l = ofi.readline()
    l = ofi.readline()
    l = ofi.readline()
    chi2 = float(l.split()[-1])
    l = ofi.readline()
    ndf = int(l.split()[-1])
    l = ofi.readline()
    scale = float(l.split()[-3])
    dscale = float(l.split()[-1])
    scalefactors[tp][bi] = {'chi2': chi2, 'SF': scale, 'dsf': dscale, 'ndf':
            ndf, 'SF+': scale + dscale, 'SF-': scale - dscale}
    l = ofi.readline()
    l = ofi.readline()
    l = ofi.readline()

sep = '\t'
qty = ['SF', 'SF+', 'SF-', 'dsf', 'chi2', 'ndf']
for ty in scalefactors:
    print "SF computed with:", ty
    #print 'eta' + ''.join(sep + qt for qt in qty)
    print 'eta' + ''.join(sep + qt for qt in qty)
    for b in scalefactors[ty]:
        print str(b) + ''.join(sep + str(scalefactors[ty][b][qt]) for qt in qty)
    print '\n'

# ETA bin 0-3 are computed with standard scales 0-3
sep = '&'
eol = '\t\\\hline'
qty = ['SF', 'SF-', 'SF+']
ty = 'standard scales'
for b in range(4):
    print str(b) + '\t' + ''.join(sep + str(round(scalefactors[ty][b][qt],3)) for qt in qty)\
            + eol


# ETA bin 4-9 are computed with standard scales 4-9 and forward scales 0-5
qty = ['SF', 'SF-', 'SF+']
tyst = 'standard scales'
tyfe = 'FE scales'
for b in range(4, 10):
    sf_standard = scalefactors[tyst][b]['SF']
    sf_standard_unc = sf_standard + scalefactors[tyst][b]['dsf']
    b_fe = b - 4
    sf_fe = scalefactors[tyfe][b_fe]['SF']
    sf_fe_unc = sf_fe + scalefactors[tyfe][b_fe]['dsf']
    sf_unc = math.sqrt(sf_standard_unc**2 + sf_fe_unc**2)
    if sf_unc == 0 or sf_standard_unc == 0 or sf_fe == 0:
        sf = 0
    else:
        sf = math.sqrt(sf_standard**2*(sf_standard_unc)**2 +
            sf_fe**2*(sf_fe_unc**2))/sf_unc
    line = str(b) + '\t' + sep
    sf_up = sf + sf_unc
    sf_down = sf - sf_unc
    for s in sf, sf_up, sf_down:
        line += str(round(s,3)) + sep
    line += eol
    print line
    #print  + ''.join(sep + str(round(scalefactors[ty][b][qt],3)) for qt in qty)\
    #        + eol



# ETA bin 10-12 are with forward scales 6-8
ty = 'FE scales'
for b in range(6, 9):
    print str(b+10) + '\t'  + ''.join(sep + str(round(scalefactors[ty][b][qt],3)) for qt in qty)\
            + eol


bin_edges = {}

bin_edges[0] = "0.0& 0.5"
bin_edges[1] = "0.5& 0.8"
bin_edges[2] = "0.8& 1.1"
bin_edges[3] = "1.1& 1.3"
bin_edges[4] = "1.3& 1.7"
bin_edges[5] = "1.7& 1.9"
bin_edges[6] = "1.9& 2.1"
bin_edges[7] = "2.1& 2.3"
bin_edges[8] = "2.3& 2.5"
bin_edges[9] = "2.5& 2.8"
bin_edges[10] = "2.8& 3.0"
bin_edges[11] = "3.0& 3.2"
bin_edges[12] = "3.2& 4.7"







out_file = 'scale_factors_use_more_forward.tex'
outopen = open(out_file, 'w')
# ETA bin 0-3 are computed with standard scales 0-3
# ETA bin 4-9 are computed with standard scales 4-9 and forward scales 0-5
# ETA bin 10-12 are with forward scales 6-8
bin_combine_min = 4
bin_combine_max = 9
tyst = 'standard scales'
tyfe = 'FE scales'
eol = '\t\\\\\hline'

for b in range(0, 13):
    if b < 11:

        sf_standard = scalefactors[tyst][b]['SF']
        sf_standard_unc = scalefactors[tyst][b]['dsf']
        sf = sf_standard
        sf_unc = sf_standard_unc
        computation = "standard"
    if b >= bin_combine_min:

        b_fe = b - 4
        sf_fe = scalefactors[tyfe][b_fe]['SF']
        sf_fe_unc = scalefactors[tyfe][b_fe]['dsf']
        sf_unc = sf_fe_unc
        computation = "forward ext."
        sf = sf_fe
        sf_unc = sf_fe_unc
        if b <= bin_combine_max and b not in [5, 6, 7]:

            # Then combine all overlapping bins weighted, like
            # sqrt(standard**2*(1/dstandard)**2 + forward**2*(1/dforward)**2) * 1/sumuncertainties(?)
            sf_unc = math.sqrt(sf_standard_unc**2 + sf_fe_unc**2)
            if sf_unc == 0 or sf_standard == 0 or sf_fe == 0:
                sf = 0
                sf_unc = 0
                computation = "None"
            else:
                sf_unc_inv = math.sqrt(1/sf_standard_unc**2 + 1/sf_fe_unc**2)
                sf = math.sqrt(sf_standard**2*(1/sf_standard_unc)**2 +
                    sf_fe**2*(1/sf_fe_unc**2))/sf_unc_inv
                computation = "standard + forward ext."
    line = bin_edges[b] + '\t' + sep
    line += str(round(sf, 3)) + sep
    line += ' $\pm$ ' + str(round(sf_unc, 3)) + sep
    line += computation
    line += eol + '\n'
    outopen.write(line)
    #print  + ''.join(sep + str(round(scalefactors[ty][b][qt],3)) for qt in qty)\
    #        + eol

outopen.close()



