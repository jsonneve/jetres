"""
Save scale factors that are printed by iterFit.C
in a latex table format.

Note that you have the following scale factors:
    Standard 0-10
    FE Control 0-9
    Standard 0.15 0-10
    Forward Extension 0.15a 0-8
    Forward Extension 0-8

Use only bins 0-9 for the Standard (10 is shitty due
to statistics).
Also, Forward starts at eta 1.3 so you should shift the
bin numbers by 4.
Then combine all overlapping bins weighted, like
sqrt(standard**2*(1/dstandard)**2 + forward**2*(1/dforward)**2) * 1/sumuncertainties(?)
"""

import math
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import sys
import os

datalabel = str(os.environ['LABELDATA'])
mclabel = str(os.environ['LABELMC'])
dataruns = str(os.environ['DATARUNS'])
print("labels:", datalabel, mclabel, dataruns)
if datalabel == '':
    datalabel = 'data'
if mclabel == '':
    mclabel = 'MC'



def weighted_average(a, da, b, db):
    """
    Return the weighted average of the two measurements.
    >>> wt = weighted_average(10, 0.5, 12, 1)
    >>> (round(wt[0], 1), round(wt[1], 1))
    (10.4, 0.4)
    """
    wa = 1/da**2
    wb = 1/db**2
    sumw = wa + wb
    combined = (a*wa + b*wb)/sumw
    combined_uncertainty = 1/math.sqrt(sumw)
    return combined, combined_uncertainty


listfi = ['scale_factors.txt']
#lumi = 804.2
lumi = 2.6
if len(sys.argv) > 1:
    lumi = sys.argv[1]
if len(sys.argv) > 2:
    listfi = sys.argv[2:]

# Read out scale factors from JER output
for fi in listfi:
    ofi = open(fi, 'r')
    scalefactors = {}
    l = ofi.readline()
    while l != '' and not '(int)0' in l:
        while 'scales,' not in l and l != '' and not '(int)0' in l:
            l = ofi.readline()
        tp, bi = None, None
        if ', eta bin' in l:
            tp, bi = l.split(', eta bin ')
            if tp not in scalefactors:
                scalefactors[tp] = {}
            bi = int(bi)

        l = ofi.readline()
        chi2 = None
        ndf = None
        scale = 0
        dscale = 0
        while 'Chi2' not in l and 'scales' not in l and l != '':
            l = ofi.readline()
        if 'Chi2' in l:
            chi2 = float(l.split()[-1])
        while 'NDf' not in l and 'scales' not in l and l != '':
            l = ofi.readline()
        if 'NDf' in l:
            ndf = int(l.split()[-1])
        while 'p0' not in l and 'scales' not in l and l != '':
            l = ofi.readline()
        if 'p0' in l:
            scale = float(l.split()[-3])
            dscale = float(l.split()[-1])
        if tp != None:
            scalefactors[tp][bi] = {'chi2': chi2, 'SF': scale, 'dsf': dscale, 'ndf':
                    ndf}
    ofi.close()

    sep = '\t&'
    eol = '\t\\\hline'

    bin_edges = {}
    bin_edges[0] = "0.0& 0.5"
    bin_edges[1] = "0.5& 0.8"
    bin_edges[2] = "0.8& 1.1"
    bin_edges[3] = "1.1& 1.3"
    bin_edges[4] = "1.3& 1.7"
    bin_edges[5] = "1.7& 1.9"
    bin_edges[6] = "1.9& 2.1"
    bin_edges[7] = "2.1& 2.3"
    bin_edges[8] = "2.3& 2.5"
    bin_edges[9] = "2.5& 2.8"
    bin_edges[10] = "2.8& 3.0"
    bin_edges[11] = "3.0& 3.2"
    bin_edges[12] = "3.2& 4.7"

    sf_computed_0 = {}

    # Compute weighted averages and write scale factors to files for alpha=0
    out_file = fi[:fi.rindex('.')] + '.tex'
    outopen = open(out_file, 'w')
    # ETA bin 0-3 are computed with standard scales 0-3
    # ETA bin 4-9 are computed with standard scales 4-9 and forward scales 0-5
    # ETA bin 10-12 are with forward scales 6-8
    bin_combine_min = 4
    bin_combine_max = 9
    tyst = 'standard scales'
    tyfe = 'FE scales'
    eol = '\t\\\\\hline'

    # Write in official format:
    etaplus_official = ''
    etamin_official = ''

    for b in range(0, 13):
        print("\nbin", b)
        if b < 11:


            sf_standard = scalefactors[tyst][b]['SF']
            sf_standard_unc = scalefactors[tyst][b]['dsf']
            sf = sf_standard
            sf_unc = sf_standard_unc
            computation = "standard"
            print("standard", sf_standard, sf_standard_unc)
        if b >= bin_combine_min:

            b_fe = b - 4
            sf_fe = scalefactors[tyfe][b_fe]['SF']
            sf_fe_unc = scalefactors[tyfe][b_fe]['dsf']
            sf_unc = sf_fe_unc
            computation = "forward ext."
            sf = sf_fe
            sf_unc = sf_fe_unc
            print("forward", sf_fe, sf_fe_unc)
            if b <= bin_combine_max:

                # Then combine all overlapping bins weighted, like
                # sqrt(standard**2*(1/dstandard)**2 + forward**2*(1/dforward)**2) * 1/sumuncertainties(?)
                #sf_unc = math.sqrt(sf_standard_unc**2 + sf_fe_unc**2)
                #sf_unc = math.sqrt(sf_standard_unc*sf_fe_unc/abs(sf_standard_unc -
                #    sf_fe_unc))
                if sf_unc == 0 or sf_standard == 0 or sf_fe == 0:
                    sf = 0
                    sf_unc = 0
                    computation = "None"
                else:
                    sf_unc = round(1/math.sqrt(1/sf_standard_unc**2 +
                        1/sf_fe_unc**2), 5)
                    sf_unc_inv = math.sqrt(1/sf_standard_unc**2 + 1/sf_fe_unc**2)
                    sf = round(math.sqrt(sf_standard**2*(1/sf_standard_unc)**2 +
                        sf_fe**2*(1/sf_fe_unc**2))/sf_unc_inv, 5)
                    print("weighted", weighted_average(sf_standard, sf_standard_unc, sf_fe, sf_fe_unc))
                    print("computed", sf, sf_unc)
                    print("standard", sf_standard, sf_standard_unc)
                    print("forward", sf_fe, sf_fe_unc)
                    computation = "standard + forward ext."
        sf_computed_0[b] = {'sf': sf, 'unc': sf_unc, 'method': computation, 'edges':
                bin_edges[b]}
        line = bin_edges[b] + '\t' + sep
        line += str(round(sf, 3)) + sep
        line += ' $\pm$ ' + str(round(sf_unc, 3)) + sep
        line += computation
        line += eol + '\n'
        outopen.write(line)
        #print  + ''.join(sep + str(round(scalefactors[ty][b][qt],3)) for qt in qty)\
        #        + eol

        # Write in official format like here:
        # -4.7  -3.2  3   1.226  1.081  1.371
        etamin = ''
        etamin += str(-float(bin_edges[b].split('&')[1]))
        etamin += '  ' + str(-float(bin_edges[b].split('&')[0]))
        etamin = etamin.replace('-0.0', ' 0.0')
        sfstring = '  3   '
        sfstring += str(round(sf, 3)) + '  '
        sfstring += str(round(sf - sf_unc, 3)) + '  '
        sfstring += str(round(sf + sf_unc, 3)) + '\n'
        etamin += sfstring
        etamin_official = etamin + etamin_official
        etaplus_official += ' ' + str(float(bin_edges[b].split('&')[0])) 
        etaplus_official += '   ' + str(float(bin_edges[b].split('&')[1]))
        etaplus_official += sfstring


    outopen.close()
    official_out = open(fi[:fi.rindex('.')] + '_official_format.txt', 'w')
    official_out.write(etamin_official)
    official_out.write(etaplus_official)
    official_out.close()
    print(etamin_official + etaplus_official)





    ### Replace scale factors in Mikko's plot script
    standard = '{{' + '{'.join(str(round(scalefactors[tyst][b]['SF'],3)) + ',' +
            str(round(scalefactors[tyst][b]['dsf'],3)) + '},' for b in
            list(range(11))) + '{0,0},{0,0}};'
    forward = '{{0,0},{0,0},{0,0},{0,0},{' + '{'.join(
            str(round(scalefactors[tyfe][b]['SF'],3)) + ',' +
            str(round(scalefactors[tyfe][b]['dsf'],3)) + '},' for b in
            list(range(9)))[:-1] + '};'
    minuncert = '{' + ''.join(str(round(scalefactors[tyst][b]['dsf'],3)) + ',' for b
            in list(range(4)))
    minuncert += ''.join(str(min(round(scalefactors[tyfe][b - 4]['dsf'],3),
        round(scalefactors[tyst][b]['dsf'],3))) + ',' for b in list(range(4, 10)))
    minuncert += ''.join(str(round(scalefactors[tyfe][b - 4]['dsf'],3))  + ','
            for b in list(range(10, 13)))[:-1] + '};'
    print(fi)
    print("Standard")
    print(standard)
    print("Forward")
    print(forward)
    print("Min. Uncert.")
    print(minuncert)
    sf_computed_15 = {}
    jer_template = open('JERplots/JERplot_template.C', 'r')
    jer_plot = open('JERplots/JERplot.C', 'w')
    line = jer_template.readline()
    while line!= '':
        line = line.replace('min_uncertainties', minuncert)
        line = line.replace('standard_scale_factors', standard)
        line = line.replace('forward_scale_factors', forward)
        line = line.replace('mcsource', fi.replace('scale_factors_', '').replace('.txt', ''))
        line = line.replace('latest_run_name', "13 TeV " + dataruns)
        line = line.replace('luminosity', str(lumi))
        jer_plot.write(line)
        line = jer_template.readline()
    jer_plot.close()
    jer_template.close()

    ### Replace lumi in Mikko's plot script
    tdr_template = open('JERplots/tdrstyle_mod15_template.C', 'r')
    tdr_plot = open('JERplots/tdrstyle_mod15.C', 'w')
    line = tdr_template.readline()
    while line!= '':
        line = line.replace('lumi_inv_fb', lumi)
        tdr_plot.write(line)
        line = tdr_template.readline()
    tdr_plot.close()
    tdr_template.close()

    # Compute weighted averages and write scale factors to files for alpha=0.15
    out_file = fi[:fi.rindex('.')] + '15.tex'
    outopen = open(out_file, 'w')
    # ETA bin 0-3 are computed with standard scales 0-3
    # ETA bin 4-9 are computed with standard scales 4-9 and forward scales 0-5
    # ETA bin 10-12 are with forward scales 6-8
    bin_combine_min = 4
    bin_combine_max = 9
    tyst = 'standard with 0.15 scales'
    tyfe = 'FE with 015 scales'
    eol = '\t\\\\\hline'

    for b in range(0, 13):
        if b < 11:

            sf_standard = scalefactors[tyst][b]['SF']
            sf_standard_unc = scalefactors[tyst][b]['dsf']
            sf = sf_standard
            sf_unc = sf_standard_unc
            computation = "standard"
        if b >= bin_combine_min:

            b_fe = b - 4
            sf_fe = scalefactors[tyfe][b_fe]['SF']
            sf_fe_unc = scalefactors[tyfe][b_fe]['dsf']
            sf_unc = sf_fe_unc
            computation = "forward ext."
            sf = sf_fe
            sf_unc = sf_fe_unc
            if b <= bin_combine_max:

                # Then combine all overlapping bins weighted, like
                # sqrt(standard**2*(1/dstandard)**2 + forward**2*(1/dforward)**2) * 1/sumuncertainties(?)
                #sf_unc = math.sqrt(sf_standard_unc**2 + sf_fe_unc**2)
                #sf_unc = math.sqrt(sf_standard_unc*sf_fe_unc/abs(sf_standard_unc -
                #    sf_fe_unc))
                if sf_unc == 0 or sf_standard == 0 or sf_fe == 0:
                    sf = 0
                    sf_unc = 0
                    computation = "None"
                else:
                    sf_unc = round(1/math.sqrt(1/sf_standard_unc**2 +
                        1/sf_fe_unc**2), 5)
                    sf_unc_inv = math.sqrt(1/sf_standard_unc**2 + 1/sf_fe_unc**2)
                    sf = math.sqrt(sf_standard**2*(1/sf_standard_unc)**2 +
                        sf_fe**2*(1/sf_fe_unc**2))/sf_unc_inv
                    computation = "standard + forward ext."
        sf_computed_15[b] = {'sf': sf, 'unc': sf_unc, 'method': computation, 'edges':
                bin_edges[b]}
        line = bin_edges[b] + '\t' + sep
        line += str(round(sf, 3)) + sep
        line += ' $\pm$ ' + str(round(sf_unc, 3)) + sep
        line += computation
        line += eol + '\n'
        outopen.write(line)
        #print  + ''.join(sep + str(round(scalefactors[ty][b][qt],3)) for qt in qty)\
        #        + eol

    outopen.close()

    pyfilename = fi[:fi.rindex('.')] + '.py'
    pyfile = open(pyfilename, 'w')
    pyfile.write("scalefactors = " + str(scalefactors) + '\n')
    pyfile.write("sf_computed_0 = " + str(sf_computed_0) + '\n')
    pyfile.write("sf_computed_15 = " + str(sf_computed_15) + '\n')
    pyfile.close()


    ## Plots
    plt.figure()
    x, y = zip(*[[i, sf_computed_0[i]['sf']] for  i in sf_computed_0])
    x = [round((float(sf_computed_0[i]['edges'].split('&')[1]) +
        float(sf_computed_0[i]['edges'].split('&')[0]))/2, 2) for i in
        range(len(sf_computed_0))]
    #xerr = [round((float(sf_computed_0[i]['edges'].split('&')[1]) -
    #    float(sf_computed_0[i]['edges'].split('&')[0]))/2, 2) for i in
    #    range(len(sf_computed_0))]
    #print('xerr wrong:', xerr)
    xerr = [round(float(sf_computed_0[i]['edges'].split('&')[1]) -
        x[i], 2) for i in range(len(sf_computed_0))]
    #print('bin_edges:', [(float(bin_edges[edg].split('&')[0]),
    #    float(bin_edges[edg].split('&')[1])) for
    #    edg in bin_edges])
    #print('x:', x)
    #print('xerr:', xerr)
    #for i in range(len(x)):
    #    print(bin_edges[i], x[i], xerr[i])
    x5, y5 = zip(*[[i, sf_computed_15[i]['sf']] for  i in sf_computed_15])
    x5 = [round((float(sf_computed_15[i]['edges'].split('&')[1]) +
        float(sf_computed_15[i]['edges'].split('&')[0]))/2, 2) for i in
        range(len(sf_computed_15))]
    xerr5 = [round(float(sf_computed_15[i]['edges'].split('&')[1]) -
        x5[i], 2) for i in range(len(sf_computed_15))]
    #xerr5 = [round((float(sf_computed_15[i]['edges'].split('&')[1]) -
    #    float(sf_computed_15[i]['edges'].split('&')[0]))/2, 2) for i in
    #    range(len(sf_computed_15))]
    #plt.xticks(x, [sf_computed_0[i]['edges'].replace('&', '$< |\eta| <$') for i in
    #    x], rotation='vertical', fontsize=16)
    #label = '$\sigma^{\mathrm{data}}_{\mathrm{JER}}/\sigma^{\mathrm{MC}}_{\mathrm{JER}})$'
    label = r'$\sigma^{\mathrm{' + datalabel + r'}}_{\mathrm{JER}}/\sigma^{\mathrm{' + mclabel + r'}}_{\mathrm{JER}})$'
    #label = r'$\sigma^{\mathrm{' + datalabel + r'}}_{\mathrm{JER(PLI)}}/\sigma^{\mathrm{' + mclabel + r'}}_{\mathrm{JER(PLI)}})$'
    print(label)
    #label5 = '$\sigma^{\mathrm{data}}_{\mathrm{0.15}}/\sigma^{\mathrm{MC}}_{\mathrm{0.15}})$'
    label5 = r'$\sigma^{\mathrm{' + datalabel + r'}}_{\mathrm{0.15}}/\sigma^{\mathrm{' + mclabel + r'}}_{\mathrm{0.15}})$'
    #label5 = r'$\sigma^{\mathrm{' + datalabel + r'}}_{\mathrm{0.15(PLI)}}/\sigma^{\mathrm{' + mclabel + r'}}_{\mathrm{0.15(PLI)}})$'
    print(label5)
    #plt.scatter(x, y, s=30, c='magenta', linewidths=0, label=label)
    plt.errorbar(x, y, xerr=xerr, yerr=[sf_computed_0[i]['unc'] for i
        in range(len(sf_computed_0))],
            ls='none', elinewidth=2, ecolor='magenta', color='magenta', fmt='--o',
            label=label)
    #plt.scatter(x5, y5, s=30, c='blue', linewidths=0, label=label5)
    plt.errorbar(x5, y5, xerr=xerr5, yerr=[sf_computed_15[i]['unc'] for i in
        range(len(sf_computed_15))],
            ls='none', elinewidth=2, ecolor='blue', color='blue', fmt='--o', label=label5)
    plt.ylim(0,2)
    plt.xlim(0,5)
    #plt.xlim(min(x) - 0.5, max(x) + 0.5)
    plt.annotate('CMS Preliminary', (0.03, 0.9), fontweight='bold', fontsize=14,
            xycoords='axes fraction')
    if 'MC' in lumi or 'data' in lumi or 'Data' in lumi:
        plt.annotate(str(lumi), (0.03, 0.8), fontsize=18, 
            xycoords='axes fraction')
    else:
        plt.annotate('$L=' + str(lumi) + '$ $\mathrm{fb}^{-1}$', (0.03, 0.8), fontsize=18, 
    #plt.annotate('$L=' + str(lumi) + '$ $\mathrm{pb}^{-1}$', (0.03, 0.8), fontsize=18, 
            xycoords='axes fraction')
    plt.ylabel('Jet energy resolution scale factor', fontsize=14)
    #plt.ylabel('PLI scale factor', fontsize=14)
    plt.xlabel('$|\eta|$', fontsize=16)
    plt.axhline(y=1, color='orange', linewidth=2, linestyle='--')
    if sys.version_info < (2, 7):
        print("You have python version", sys.version)
        print("Warning: Plots are not guaranteed to look nice for Python < 2.7!")
        plt.legend(loc='lower right')
    else:
        plt.legend(loc='lower right', fontsize=20)
    plotfilename = fi[:fi.rindex('.')] + '.pdf'
    plt.savefig(plotfilename, bbox_inches='tight')
    print("Saved file to ", plotfilename)



if __name__ == "__main__":
    import doctest
    doctest.testmod()
