#!/usr/bin/env zsh



export CURDIR=$(pwd)
cd $CURDIR

# If you do not have the program grid-control installed yet, install it somewhere with:
#    svn co https://ekptrac.physik.uni-karlsruhe.de/svn/grid-control/tags/stable/grid-control
export PATH_TO_GRID_CONTROL='/afs/desy.de/user/s/sonnevej/xxl/grid-control'

# For luminosities see also https://docs.google.com/spreadsheets/d/1U1wALjqXxqZJSw-muj_zvvZSaZ2_B3CMmIID6IT1iD4/edit#gid=0
#export LUMI_INV_FB=12.9 # Run BCD
#export LUMI_INV_FB=5.892 # Run B
#export LUMI_INV_FB=2.646 # Run C
#export LUMI_INV_FB=4.353 # Run D
#export LUMI_INV_FB=4.050 # Run E
#export LUMI_INV_FB=3.160 # Run F
export LUMI_INV_FB=7.554 # Run G
#export LUMI_INV_FB=8.496 # Run H
#export LUMI_INV_FB=27.22 #  Cert_271036-280385 Run B-H
#export LUMI_INV_FB=33.59 # Cert_271036-283685 Run B-H
#export LUMI_INV_FB=36.151 # Cert_271036-284044 Run B-H
# export LUMI_INV_FB=13.48 # 33.59-3.160-4.050-12.9 (all - bcd - f - e)
#export LUMI_INV_FB=16.04 # 36.151-3.160-4.050-12.9 (all - bcd - f - e)
export LABEL_LUMI_INV_FB=$LUMI_INV_FB' fb^{-1}'
export LABELDATA='Data'
export LABELMC='MC'
#export DATARUNS="2016BCD"
#export DATARUNS="2016GH"
export DATARUNS="2016G"
#export DATARUNS="2016H"

# MC only (e.g. for smearing):
#export LABEL_LUMI_INV_FB='[MC 80X]'

export QUOTES='"'
export LUMI=$QUOTES$LUMI_INV_FB' fb^{-1}'$QUOTES

# name of files in gridcontrol dir (will be used for all files and directories):
# export name_data="80_dataBCD"
export name_data="80_dataG_V8p2"
export name_mc= "80_pythia_htbinned_V8p2"
#"80_pythia_flat_spring16_v2_13invfb63_PU_reweighted"
#"80_pythia_htbinnedfrom100_trunc_3pct"
export pileup_data=/nfs/dust/cms/user/sonnevej/resolutions_input/pileup/dataPileupHistogram_69dot3mb_Final_Cert_271036-284044_13TeV_PromptReco_Collisions16_JSON_maxpileup100.root
# dataPileupHistogram_69dot3mb_Cert_271036-284044_13TeV_PromptReco_Collisions16_JSON_NoL1T.root
# export pileup_data=/nfs/dust/cms/user/sonnevej/resolutions_input/pileup/nPU_minBiasXsec63000_Cert_271036-276811_13TeV_PromptReco_Collisions16_JSON.root
# export pileup_mc=/nfs/dust/cms/user/sonnevej/resolutions_input/pileup/pythia_PU_spring16_v2_HTbinned.root
export pileup_mc=/nfs/dust/cms/user/sonnevej/resolutions_input/pileup/pythia_htbinned_PU_from_HT100_fine_eta_analysis_spring16_v2_13invfb_noPUreweighting.root
# herwig:
# export name_mc = "80_herwig"
# export pileup_mc = /nfs/dust/cms/user/sonnevej/resolutions_input/pileup/herwig_PU_spring16_v2.root

# Uncomment to compile code:
# ./compile_all.sh

####################
# Change the config files to point to the correct datasets and output directories:
# E.g.
#       gridcontrol/80_data.conf
#       gridcontrol/80_pythia.conf
#       gridcontrol/80_herwig.conf
# Do not leave comments around in these files!
# Directories are read out of gridcontrol config files (see below).
# Existing output will be overwritten.
####################

# Uncomment to run on datasets/mc:
# Run on data:
#$PATH_TO_GRID_CONTROL/go.py -Gci $CURDIR/gridcontrol/${name_data}.conf &&
#
## Copy pileup histograms for pileup reweighting:
cp ${pileup_data} /nfs/dust/cms/user/sonnevej/resolutions_input/pileup_data.root &&
cp ${pileup_mc} /nfs/dust/cms/user/sonnevej/resolutions_input/pileup_mc.root &&
#
## Run on MC:
#$PATH_TO_GRID_CONTROL/go.py -Gci $CURDIR/gridcontrol/${name_mc}.conf &&



# Pythia vs Herwig: e.g.
#cp hist_preparation/MC/fine_eta_bin/pileup/pythia_PU_spring16_v2.root /nfs/dust/cms/user/sonnevej/resolutions_input/pileup_data.root
#cp hist_preparation/MC/fine_eta_bin/pileup/herwig_PU_spring16_v2.root /nfs/dust/cms/user/sonnevej/resolutions_input/pileup_mc.root
#$PATH_TO_GRID_CONTROL/go.py -Gci $CURDIR/gridcontrol/80_herwig_pythiaPUreweighted.conf &&

####################
# Smeared pythia
# Do not forget to uncomment the lines in
# hist_preparation/MC/fine_eta_bin/MySelector.C
# Also, do not forget to change the smearing files in
# hist_preparation/MC/fine_eta_bin/MySelector.h
#cp /nfs/dust/cms/user/sonnevej/resolutions_input/pileup/nPU_minBiasXsec69000_Cert_271036-276811_13TeV_PromptReco_Collisions16_JSON.root /nfs/dust/cms/user/sonnevej/resolutions_input/pileup_data.root &&
#cp /nfs/dust/cms/user/sonnevej/resolutions_input/pileup/pythia_PU_spring16_v2_HTbinned.root /nfs/dust/cms/user/sonnevej/resolutions_input/pileup_mc.root &&
#$PATH_TO_GRID_CONTROL/go.py -Gci $CURDIR/gridcontrol/80_pythia_htbinned_smeared.conf &&
####################
echo Done running grid-control!

# uncomment to hadd all files:
for origin in ${name_data} ${name_mc}
#for origin in pythia_htbinned_smeared #data pythia herwig #smeared_data smeared_pythia13
    do
        # Collect histograms
        export OUTDIR=$(grep 'se path' gridcontrol/${origin}.conf | cut -b18-)
        for histname in histograms PU pt
            do
                hadd $OUTDIR/${origin}_${histname}_incl_full.root $OUTDIR/*job*_${histname}_*.root
            done

    done


## Plot scale factors and save output of fits
cd JER/fine_eta_binning/
export OUTDIR=$(grep 'se path' ../../gridcontrol/${name_data}.conf | cut -b18-)
export DATAFILE=$OUTDIR/${name_data}_histograms_incl_full.root
export OUTDIR=$(grep 'se path' ../../gridcontrol/${name_mc}.conf | cut -b18-)
export MCFILE=$OUTDIR/${name_mc}_histograms_incl_full.root
echo "Will run plotting on data and mc files:"
echo $DATAFILE
echo $MCFILE


###############
# look at smeared vs unsmeared
#export LUMI_INV_FB='MC80X'
#export LABELDATA='Smeared MC'
###############
for mc in ${name_mc}
    do
        export ORIGIN=${mc}_vs_${name_data} &&
        ./plot_data_mc.sh > ../../postanalysis/scale_factors_${ORIGIN}.txt &&
        echo wrote to file ../../postanalysis/scale_factors_${ORIGIN}.txt
    done
cd ../..


echo "Ran plotting on data and mc files:"
echo $DATAFILE
echo $MCFILE
# You may want to run this on a machine/in an environment with enough python support
./sf_plots.sh
cd postanalysis/JERplots
./plot_JER.sh
cd ../..
echo You may want to run plotting on a machine or in an environment with enough python support
echo If the plots came out bad, use these variables and run plotting:
echo export ORIGIN=${ORIGIN}
echo export LUMI_INV_FB=${LUMI_INV_FB}
echo export LABELDATA=${LABELDATA}
echo export LABELMC=${LABELMC}
echo export DATARUNS=${DATARUNS}
echo ./sf_plots.sh

echo "Ran plotting on data and mc files:"
echo $DATAFILE
echo $MCFILE
# You may want to run this on a machine/in an environment with enough python support
