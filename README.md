#### Running histogram preparation ####

#### If you do not have the program grid-control installed yet:
svn co https://ekptrac.physik.uni-karlsruhe.de/svn/grid-control/tags/stable/grid-control

### To run everything:
#### Uncomment all parts you want to run in run_all.sh
#### Make sure to set the correct lumi there, the location of grid control,
#### and the grid control configuration files that point to the datasets in
#### the gridcontrol directory (80_data.conf, 80_pythia.conf, 80_herwig.conf).
./run_all.sh

#### This config file calls the following executable:
gridcontrol/run.sh
#### In this file, the $FILE_NAMES are already defined in grid-control
#### Each timeone filename is written to filelist and the following is executed:
./Analysis_incl_full.x filelist


### Compile
./compile_all.sh

### Test MC:
cd hist_preparation/MC/fine_eta_bin/
./Analysis_incl_full.x ../lists/file_list_25ns_80X_test.txt
mv histograms_mc_incl_fine.root ../../../histograms
cd ../../..
#### and data:
cd hist_preparation/data/fine_eta_bin/
./Analysis_inc_full.x testfilelist.txt
mv histograms_data_incl_fine.root ../../../histograms
cd ../../..


### Run grid-control:
#### To submit one gridcontrol job:
export PATH_TO_GRID_CONTROL=/path/to/your/grid-control_code
#### Run on e.g. data:
$PATH_TO_GRID_CONTROL/go.py -Gci gridcontrol/80_dataG_V8p2.conf &&
#### More examples:
/your/path/to/grid-control/go.py -Gci gridcontrol/80_data.conf

/your/path/to/grid-control/go.py -Gci gridcontrol/80_pythia.conf

/your/path/to/grid-control/go.py -Gci gridcontrol/80_herwig.conf


#### Plotting ####
cd JER/fine_eta_binning/
#### Watch to use batch mode (or else takes very long for generating pdfs!)
./plot_data_mc.sh
root -b
#### Compile
.L iterFit.C++
mainRun(false,"/path/to/histograms/histograms_mc_incl_full_fine.root","/path/to/histograms/histograms_data_incl_full.root")



### Pileup reweighting ###
In order to do pileup reweighting, a 'data pileup' and 'mc pileup' histogram are compared.
Currently the location of these is hardcoded in hist_preparations/MC/fine_eta_bin/MySelector.h

The pileup histogram from MC can be obtained by hadding the output of gridcontrol on unweighted pileup MC.

In order to not use pileup reweighting, just copy the same pileup histogram to the data and MC pileup histograms so that all weights are 1.

To obtain a pileup histogram for data:
1. Log in to lxplus.

2. Clone the REcoLuminosity LumiDB package:
git clone https://github.com/cms-sw/RecoLuminosity-LumiDB.git

3. Go to the scripts directory:
cd RecoLuminosity-LumiDB/scripts

4. To create a pileup histogram:
* Find the latest JSON file;
* Decide on your minimum bias cross section (here 69mb; subject to many changes);
* Decide on number of bins desired (here 60).

5. Now run (replace as appropriate):
./pileupCalcHard.py -i /afs/cern.ch/cms/CAF/CMSCOMM/COMM_DQM/certification/Collisions16/13TeV/Cert_271036-276811_13TeV_PromptReco_Collisions16_JSON.txt --inputLumiJSON /afs/cern.ch/cms/CAF/CMSCOMM/COMM_DQM/certification/Collisions16/13TeV/PileUp/pileup_latest.txt --calcMode true --minBiasXsec 69200 --maxPileupBin 60 --numPileupBins 60 dataPileupHistogram_69mb.root


Check that after reweighting the MC pileup and data pileup agree.

#### See also
https://twiki.cern.ch/twiki/bin/viewauth/CMS/PileupJSONFileforData#Pileup_JSON_Files_For_Run_II

https://twiki.cern.ch/twiki/bin/viewauth/CMS/PileupJSONFileforData#Calculating_Your_Pileup_Distribu


## See also run_all.sh
