import os
import ROOT as rt
import glob

dust_dir = '/nfs/dust/cms/user/sonnevej/'
pnfs_dir = '/pnfs/desy.de/cms/tier2/store/user/jsonneve/'
qcd_ht_gt_200 = pnfs_dir + 'JMEValidatorTree_13TeV_MC_TuneCUETP8M1_13TeV-madgraphMLM-pythia8_RunIISpring16MiniAODv2-PUSpring16_80X_mcRun2_asymptotic_2016_miniAODv2_v0_ext1-v1_MINIAODSIM'
qcd_ht_lt_200 = dust_dir + 'GridControl_JEC/JMEValidatorTree_13TeV_MC_TuneCUETP8M1_13TeV-madgraphMLM-pythia8_RunIISpring16MiniAODv2-PUSpring16_80X_mcRun2_asymptotic_2016_miniAODv2_v0_ext1-v1_MINIAODSIM_HTlt200'
#'GridControl_JEC/JMEValidatorTree_13TeV_MC_TuneCUETP8M1_13TeV-madgraphMLM-pythia8_RunIISpring16MiniAODv2-PUSpring16_80X_mcRun2_asymptotic_2016_miniAODv2_v0_ext1-v1_MINIAODSIM_HTlt500'
#qcd_ht_to_1000 = dust_dir + 'GridControl_JEC/JMEValidatorTree_13TeV_MC_TuneCUETP8M1_13TeV-madgraphMLM-pythia8_RunIISpring16MiniAODv2-PUSpring16_80X_mcRun2_asymptotic_2016_miniAODv2_v0_ext1-v1_MINIAODSIM'
#qcd_ht_gt_1000 = dust_dir + 'GridControl_JEC/JMEValidatorTree_13TeV_MC_TuneCUETP8M1_13TeV-madgraphMLM-pythia8_RunIISpring16MiniAODv2-PUSpring16_80X_mcRun2_asymptotic_2016_miniAODv2_v0_ext1-v1_MINIAODSIM_HTgt1000'

print("Will find number of events in the files in", qcd_ht_lt_200)
print("and", qcd_ht_gt_200)

#directories = [qcd_ht_to_1000, qcd_ht_gt_1000]
#list_of_files = glob.glob(qcd_ht_to_1000/*.root)
#list_of_files += glob.glob(qcd_ht_gt_1000/*.root)


qcd = {}

qcd["HT100to200"]   = {}
qcd["HT200to300"]   = {}
qcd["HT300to500"]   = {}
qcd["HT500to700"]   = {}
qcd["HT700to1000"]  = {}
qcd["HT1000to1500"] = {}
qcd["HT1500to2000"] = {}
qcd["HT2000toInf"]  = {}
qcd["HT100to200"]["xsec"]   = 27990000 #2.799e+07
qcd["HT200to300"]["xsec"]   = 1717000
qcd["HT300to500"]["xsec"]   = 351300
qcd["HT500to700"]["xsec"]   = 31630
qcd["HT700to1000"]["xsec"]  = 6802
qcd["HT1000to1500"]["xsec"] = 1206
qcd["HT1500to2000"]["xsec"] = 120.4
qcd["HT2000toInf"]["xsec"]  = 25.24

for key in qcd:
    print("Looking at HT samples:", key)
    qcd[key]['nentries'] = 0
    print("Will now look for files matching\n", qcd_ht_lt_200 + '/*' + key +
            '*.root')
    print("or matching\n", qcd_ht_gt_200 + '/*' + key + '*.root')
    list_of_files = glob.glob(qcd_ht_lt_200 + '/*' + key + '*.root')
    list_of_files += glob.glob(qcd_ht_gt_200 + '/*' + key + '*.root')
    #list_of_files = glob.glob(qcd_ht_to_1000 + '/*' + key + '*.root')
    #list_of_files += glob.glob(qcd_ht_gt_1000 + '/*' + key + '*.root')
    for filename in list_of_files:
        print("Looking at file:", filename)
        #filename = "/home/sonnevej/workspace/JMEValidatorTree_13TeV_DATA_Run2016C_Cert_271036-276811_13TeV_PromptReco_Collisions16_JSON_NoL1T_Spring16_25nsV6_L2L3Res_dataJetHTRun2016C_256.root"
        # Check if file exists
        if not os.path.exists(filename):
            print("WARNING: file does not exist!", filename)
            continue
        # Open file with ROOT
        tfile = rt.TFile(filename)
        # If you want, print the keys in the file
        #print([key for key in tfile.GetListOfKeys()])
        # OUT: [<ROOT.TKey object ("event") at 0x29f6380>, <ROOT.TKey object ("hlt") at 0x29f64a0>, <ROOT.TKey object ("vertex") at 0x29f65c0>, <ROOT.TKey object ("muons") at 0x29f66e0>, <ROOT.TKey object ("electrons") at 0x29f6800>, <ROOT.TKey object ("photons") at 0x29f8f20>, <ROOT.TKey object ("AK4PFPUPPI") at 0x1ae9a40>, <ROOT.TKey object ("AK4PFCHS") at 0x29d3c60>, <ROOT.TKey object ("AK4PF") at 0x1aec800>, <ROOT.TKey object ("met") at 0x21a56a0>, <ROOT.TKey object ("met_chs") at 0x21a5510>, <ROOT.TKey object ("met_puppi") at 0x29fde00>, <ROOT.TKey object ("puppiReader") at 0x293f760>]


        # That is how we know to what name to ask for: the event key:
        tevent = tfile.Get("event")

        # If you want, print the keys in the event key:
        #print([key for key in tevent.GetListOfKeys()])
        # OUT: [<ROOT.TKey object ("t") at 0x2d3e220>]

        # That is how we know that we should get the tree t (I just happen to now
        # it's a tree):
        events = tevent.Get("t")

        # Get the number of entries in the tree:
        nentries = events.GetEntries()
        print("File has number of events:", nentries)
        qcd[key]['nentries'] += nentries


        # Print nentries if you want:
        # nentries
        # OUT: 92259L
        # type(nentries)
        # OUT: <type 'long'>
        # int(nentries)
        # OUT: 92259
    print("number of entries for sample", key, "is:", qcd[key]["nentries"])
    print("xsec of sample", key, "is:", qcd[key]["xsec"])


print("Done!")
for key in qcd:
    print("number of entries for sample", key, "is:", qcd[key]["nentries"])
    print("xsec of sample", key, "is:", qcd[key]["xsec"])
    print("weight of sample", key, "is:", qcd[key]["xsec"]/qcd[key]["nentries"])
for key in qcd:
    s = 'if ( filename.std::string::find("'
    s += key + '") != std::string::npos)    return '
    s += str(qcd[key]["xsec"]) + '/' + str(qcd[key]["nentries"]) + '.;'
    print('\n' + s + '\n')


