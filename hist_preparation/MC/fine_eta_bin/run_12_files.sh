#!/usr/bin/env zsh

for fileno in 1 2 # 3 4 5 6
    do
        #echo $fileno
        sed "s/int i = fillyourfileno/int i = ${fileno}/g" Analysis.C.orig > Analysis.C
        sed "s/fillyourfileno/${fileno}/g" MySelector.C.orig > MySelector.C
        sed "s/fillyourfileno/${fileno}/g" Makefile.orig > Makefile
        make &&
        ./Analysis_incl_full_${fileno}.x
        #for fi in *.root
        for fi in file${fileno}*.root
            do
                #echo $fi
                #cp $fi ../../../rootfiles/res_output/file${fileno}_$fi
                cp $fi ../../../rootfiles/res_output/
            done
    done
