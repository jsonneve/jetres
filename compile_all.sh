#!/usr/bin/env zsh


#### Compile MC
cd hist_preparation/MC/fine_eta_bin/
make clean
make
cd ../../..
# and data:
cd hist_preparation/data/fine_eta_bin/
make clean
make
cd ../../..


