#!/usr/bin/env zsh

if [[ -a "pdfy" ]]
    then
        echo dir pdfy already exists
    else
        mkdir pdfy
        echo "created dir pdfy"
fi
export QUOTES='"'
export MC_LABEL=$QUOTES$LABELMC$QUOTES
export DATA_LABEL=$QUOTES$LABELDATA$QUOTES
export MC=$QUOTES$MCFILE$QUOTES
export DATA=$QUOTES$DATAFILE$QUOTES
#export LUMI_LABEL=$QUOTES$LUMI_INV_PB' pb^{-1}'$QUOTES
#export LUMI_LABEL=$QUOTES$LUMI_INV_FB' fb^{-1}'$QUOTES
export LUMI_LABEL=$QUOTES$LABEL_LUMI_INV_FB$QUOTES

echo "Using data file "$DATA
echo "Using MC file "$MC
echo "Using data label "$DATA_LABEL
echo "Using MC label "$MC_LABEL
root -b -l << EOF
.L iterFit.C++
mainRun(false,${MC},${DATA},${LUMI_LABEL},${MC_LABEL},${DATA_LABEL}) 
.q
EOF
if [[ -a "plots_${ORIGIN}" ]]
    then
        echo dir plots_$ORIGIN already exists
        echo will overwrite all files
        cp -p pdfy/* plots_$ORIGIN
    else
        mv pdfy plots_$ORIGIN
        cp -p output/* plots_$ORIGIN
        echo "created dir plots_$ORIGIN"
fi
