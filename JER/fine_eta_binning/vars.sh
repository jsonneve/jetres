#!/usr/bin/env zsh

export LABELMC="Herwig++"
export LABELDATA="Pythia8"
export DATA=/afs/desy.de/user/s/sonnevej/res/output_res_mc_pythia_fine_eta_analysis_spring16_v2_weighted/pythia_histograms_mc_incl_full.root
export MC=/afs/desy.de/user/s/sonnevej/res/output_res_mc_herwig_fine_eta_analysis_spring16_v2_pythiaPUreweighted/herwig_histograms_mc_incl_full.root
export LUMI_INV_FB="[MC 80X]"
