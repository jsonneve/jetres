#!/usr/bin/env zsh

if [[ -a "pdfy" ]]
    then
        echo dir pdfy already exists
    else
        mkdir pdfy
        echo "created dir pdfy"
fi
export QUOTES='"'
export LABELMC="Smeared MC"
export LABELDATA="MC"
export MCFILE=/nfs/dust/cms/user/sonnevej/res_output/output_res_mc_pythia_fine_eta_analysis_spring16_v2_13invfb63_PU_reweighted_smeared/smeared_pythia13_histograms_incl_full.root
#/nfs/dust/cms/user/sonnevej/res_output/output_res_mc_pythia_fine_eta_analysis_spring16_v2_13invfb63_PU_reweighted/pythia_histograms_incl_full.root
export DATAFILE=/nfs/dust/cms/user/sonnevej/res_output/output_res_mc_pythia_fine_eta_analysis_spring16_v2_13invfb63_PU_reweighted/pythia_histograms_incl_full.root
#/nfs/dust/cms/user/sonnevej/res_output/output_res_data_fine_eta_analysis_Run2016BCD_Cert_271036-276811_13TeV_PromptReco_Collisions16_JSON_NoL1T_Spring16_25nsV6_L2L3Res/data_histograms_incl_full.root
export LUMI_INV_FB="[MC 80X]"
export LABEL_LUMI_INV_FB="[MC 80X]"
export MC_LABEL=$QUOTES$LABELMC$QUOTES
export DATA_LABEL=$QUOTES$LABELDATA$QUOTES
export MC=$QUOTES$MCFILE$QUOTES
export DATA=$QUOTES$DATAFILE$QUOTES
#export LUMI_LABEL=$QUOTES$LUMI_INV_PB' pb^{-1}'$QUOTES
#export LUMI_LABEL=$QUOTES$LUMI_INV_FB' fb^{-1}'$QUOTES
#export LUMI_LABEL=$QUOTES$LABEL_LUMI_INV_FB' fb^{-1}'$QUOTES
export LUMI_LABEL=$QUOTES$LABEL_LUMI_INV_FB$QUOTES
export ORIGIN="SF13invfb_smeared_pythia_vs_pythia"

echo "Using data file "$DATA
echo "Using MC file "$MC
echo "Using data label "$DATA_LABEL
echo "Using MC label "$MC_LABEL
root -b -l << EOF
.L iterFit.C++
mainRun(false,${MC},${DATA},${LUMI_LABEL},${MC_LABEL},${DATA_LABEL}) 
.q
EOF
if [[ -a "plots_${ORIGIN}" ]]
    then
        echo dir plots_$ORIGIN already exists
        echo will overwrite all files
        cp -p pdfy/* plots_$ORIGIN
    else
        mv pdfy plots_$ORIGIN
        cp -p output/* plots_$ORIGIN
        echo "created dir plots_$ORIGIN"
fi
